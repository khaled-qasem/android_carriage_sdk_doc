# Carriage Games SDK
[![Version]
[![License]

## Inroduction 

Gamiphy provides an inventory of game templates that is available for businesses to customize, change its brands and publish anyware, on the web, social media, within the mobile apps. 
It also support custom integrations with specific technologies and support the clients to have a smooth process of integration. 
This document is prepared to explain the basics of the requirments, and the main components needed to integrate a Gamiphy Carriage game within an Android application. 

## Requirements

- Android Studio 3.1+
- Java 1.8
- Androidx

## Installation

gamiphy is available through [JitPack](https://jitpack.io/). To install
it, simply add the dependency for the Gamiphy SDK in your module (app-level) Gradle file (usually app/build.gradle):

```gradle
       // Todo : change this to gamiphy repo
       implementation 'com.github.gamiphy:CarriageSDK:0.0.1'
```

and make sure you have jitpack in your root-level (project-level) Gradle file (build.gradle), 
```gradle
   allprojects {
    repositories {
        google()
        jcenter()
        // add jitpack if it's not added
        maven { url 'https://jitpack.io' }
    }
}
```


## Showing the game within your application

Carriage game can be triggered and shown in your activity. 
if you want to open the game after a certin action. you can do so by calling the following method: 

```Kotlin
    GamiphySDK.getInstance().open(context, User(name,email,hash))
```

## Tracking delivery time

Carriage game support the ability to track delivery time. 
and to track it you need to call this function. 

```kotlin
GamiBot.getInstance().trackDelivery()
```

Carriage SDK Listeners:

- OnGameDone: this listener has onGameDone method,this method called when the game finished.
counter: count number of games 
```kotlin
 carriageSDK.registerOnGameDone(object : OnGameDone {
            override fun onGameDone(counter: int) {
                Log.d(MainActivity::class.java.simpleName, "here is counter $counter")
            }
        })
```
